﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StickHeroController : MonoBehaviour
{
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private StickHeroPlatform[] m_Platforms;

    private int counter; //счетчик платформ
    private enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeat,
    }

    private EGameState curEGameState;
    
    public void StopStickScale()
    {
        curEGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }

    //?выкинуть
    public void StoStickRotate()
    {
        curEGameState = EGameState.Movement;
        
    }

    public void StartPlayerMovement(float length)
    {
        curEGameState = EGameState.Movement;
        StickHeroPlatform nextPlatform = m_Platforms[counter + 1];

        //находим минимальную длину стика для успешного перехода
        float targetLength = nextPlatform.transform.position.x - m_Stick.transform.position.x;
        float platformSize = nextPlatform.GetPlatformSize();
        float min = targetLength - platformSize * 0.5f;
        min -= m_Player.transform.localScale.x;

        //находим максимальную длину стика для стика
        float max = targetLength + platformSize * 0.5f;

        //при успехе игрок переходит в центр платформы, иначе падаем

        if (length < min || length > max)
        {
            float targetPosition = m_Stick.transform.position.x + length + m_Player.transform.localScale.x;
            m_Player.StartMovement(targetPosition, true);
        }
        else
        {
            float targetPosition = nextPlatform.transform.position.x;
            m_Player.StartMovement(targetPosition, false);
        }

    }
    
    public void StopPlayerMovement()
    {
        curEGameState = EGameState.Wait;
        counter++;
        m_Stick.ResetStick(m_Platforms[counter].GetStickPosition());
    }

    public void ShowScores()
    {
        curEGameState = EGameState.Defeat;
        print($"Game over {counter}");
    }
    
    
    // Start is called before the first frame update
    private void Start()
    {
        curEGameState = EGameState.Wait;
        counter = 0;
        m_Stick.ResetStick(m_Platforms[0].GetStickPosition());
    }

    // Update is called once per frame
    private void Update()
    {
        if (!Input.GetMouseButtonDown(0)) return;

        switch (curEGameState)
        {
            //если не нажата кнопка старт
            case EGameState.Wait:
                curEGameState = EGameState.Scaling;
                m_Stick.StartScaling();
                break;
            //стик увеличивается - прерываем увеличением и запускаем поворот
            case EGameState.Scaling:
                curEGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                break;
            //ничего не делать
            case EGameState.Rotate:
                break;
            case EGameState.Movement:
                break;
            //перезапускаем игру
            case EGameState.Defeat:
                print("Game restart");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

}

//дз перечитать код .. можно задавать вопросы в телеграм .. сделать бесконечную генерацию платформ с правильным расчетом положения и падения игрока