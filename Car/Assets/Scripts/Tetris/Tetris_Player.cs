﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Networking.PlayerConnection;
using UnityEngine;

public class Tetris_Player : MonoBehaviour
{
    [SerializeField] private GameObject[] m_player;
    private float numberControler = 0f;
    private GameObject player;
    private GameObject nextPlayer;
    // Start is called before the first frame update
    private void Start()
    {
        NowPlayer();
        
        //player = GameObject.CreatePrimitive(PrimitiveType.Cube); 
        //player.transform.position = new Vector3(-1f,0f,10.5f);

        //nextPlayer = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //nextPlayer.transform.position = new Vector3(5.5f,0f,0f);
    }
    
    private void NextPlayer()
    {
        int randomNextPlayer = Random.Range(0, m_player.Length);
       // Instantiate(m_player[randomNextPlayer], transform.position, Quaternion.identity);
       Instantiate(m_player[randomNextPlayer], new Vector3(4.5f,0f,0f), Quaternion.identity);
       numberControler = 1f;
    }

    private void NowPlayer()
    {
        if (numberControler < 1f)
        {
            int randomNowPlayer = Random.Range(0, m_player.Length);
            player = Instantiate(m_player[randomNowPlayer], new Vector3(-1f,0f,10.5f), Quaternion.identity);
            NextPlayer();
        }

        else
        {
            player = nextPlayer;
            player.transform.position = new Vector3(-1f,0f,10.5f);
            NextPlayer();
        }
    }
}
