﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetris_MapCreator : MonoBehaviour
{
    //[SerializeField] private GameObject m_mapLeftWall;
    //[SerializeField] private GameObject m_mapRighttWall;
    //[SerializeField] private GameObject m_mapPlane;
    private GameObject mapLeftWall;
    private GameObject mapRighttWall;
    private GameObject mapPlane;

    [SerializeField] private float m_lenghtWall = 40f;

    //[SerializeField] private int m_mapWallDistance = 10f;
    
    // Start is called before the first frame update
    void Start()
    {
        mapLeftWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        mapLeftWall.transform.localScale = new Vector3(0.1f, 0.1f, m_lenghtWall);
        mapLeftWall.transform.position = new Vector3(-6f,0f,0f);
        
        mapRighttWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
        mapRighttWall.transform.localScale = new Vector3(0.1f, 0.1f, m_lenghtWall);
        mapRighttWall.transform.position = new Vector3(4f, 0f, 0f);
        
        mapPlane = GameObject.CreatePrimitive(PrimitiveType.Cube);
        mapPlane.transform.localScale = new Vector3(13f, 0f, m_lenghtWall);
        mapPlane.transform.position = new Vector3(-0f,0f, 0f);
        
    }

  }
