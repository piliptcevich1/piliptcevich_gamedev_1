﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class FitInTheHall_LevelGenerator : MonoBehaviour
{
    [SerializeField] private GameObject m_CubePrefab;
    [SerializeField] private float m_BaseSpeed = 2f;
    [SerializeField] private float m_WallDistance = 35f;
    [SerializeField] private FitInTheHole_Template[] m_templatesPrefabs;
    [SerializeField] private Transform m_FigurePoint; //точка в которой хранится фигура


    private FitInTheHole_Template[] templates; //храним экземпляры шаблонов, чтобы не инстансировать каждый раз
    private FitInTheHole_Template figure; //текущая фигура
    private float speed;

    private FitIntheHall_Wall wall; // ссылочка на сцену

    // Start is called before the first frame update
    private void Start()
    {
        templates = new FitInTheHole_Template[m_templatesPrefabs.Length];
        for (int i = 0; i < templates.Length; i++)
        {
            templates[i] = Instantiate(m_templatesPrefabs[i]);
            templates[i].gameObject.SetActive(false);
            templates[i].transform.position = m_FigurePoint.position;
        }

        wall = new FitIntheHall_Wall(5, 5, m_CubePrefab);
        SetupTemplate();
        wall.SetupWall(figure, m_WallDistance);
        speed = m_BaseSpeed;
    }

    // Update is called once per frame
    private void Update()
    {
        wall.Parent.transform.Translate(speed * Time.deltaTime * Vector3.back);
        if (wall.Parent.transform.position.z > m_WallDistance * -1f)
        {
            return;
        }

        SetupTemplate();
        wall.SetupWall(figure, m_WallDistance); //перезапуск стены
    }

    private void SetupTemplate()
    {
        if (figure)
        {
            figure.gameObject.SetActive(false);    
        }

        var rand = UnityEngine.Random.Range(0, templates.Length);
        figure = templates[rand];
        figure.gameObject.SetActive(true);
        figure.SetupRandomFigure(); //мы переиспользуем шаблон, поэтому нужно проверять отключение всех шаблонов
    }
}
