﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class Exam_Wall : MonoBehaviour
{
  private List<Transform> cubes;

    private Transform parent;

    public Transform Parent => parent;

    public Exam_Wall(int sizeX, int sizeY, GameObject prefab)
    {
        //создаем конструктор класса типа геймобжэект
        GenerateWall(sizeX, sizeY, prefab);
    }

    private void GenerateWall(int sizeX, int sizeY, GameObject prefab)
    {
        cubes = new List<Transform>();
        parent = new GameObject().transform;

        for (int x = -sizeX + 1; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                var obj = Object.Instantiate(prefab, new Vector3(x, y,0), Quaternion.identity); //если нужно инстантировать объект нулевой поворот чеерз Qaternion
                obj.transform.parent = parent;
                cubes.Add(obj.transform);
            }
        }
        parent.position = new Vector3(0f, 0.5f, 0f); //настройка координаты родителя-префаба (поднимаем над трассой, чтобы не проваливались дети под землю)
    }

    public void SetupWall(Exam_Template template, float position)
    {
        parent.transform.position = new Vector3(0f, 0f, position);
        foreach (var cube in cubes)
        {
            cube.gameObject.SetActive(true);
        }

        if (template == null)
        {
            return;
        }

        Transform[] figure = template.GetFigure();
        for (int f = 0; f < figure.Length; f++)
        {
            for (int c = 0; c < cubes.Count; c++)
            {
                if (!figure[f] || !cubes[c])
                {
                    continue;
                }
                if (Mathf.Abs(figure[f].position.x - cubes[c].position.x) > 0.1f)
                {
                    continue;
                }
                if (Mathf.Abs(figure[f].position.y - cubes[c].position.y) > 0.1f)
                {
                    continue;
                }
                cubes[c].gameObject.SetActive(false); //сравниванием все точки фигуры с расстоянием от центра каждого кубика стены .. если совпадают, тогда кубик в стенке отключить
                //фигуры идентичны кубам, вырезанным из стены . поэтому легко сравнить их центры
            }   
        }
    }
}
