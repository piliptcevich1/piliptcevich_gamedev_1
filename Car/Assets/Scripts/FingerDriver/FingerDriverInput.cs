﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverInput : MonoBehaviour
{
    [SerializeField] private Transform m_steerWeelTransform;
    [SerializeField] [Range(0, 180f)] private float m_maxSteerAngle = 90; //максимальный угол поворота рулевого колеса
    [SerializeField] [Range(0f, 1f)] private float m_steerAcceleration = 0.25f; // плавность поворота и движений

    private float steerAxis;

    public float SteerAxis
    {
        get => steerAxis; //сокращение от return steerAxis;
        set => steerAxis = Mathf.Lerp(steerAxis, value, m_steerAcceleration);
    }

    private Vector2 startSteerWheelPoint; //стартовая точка колеса
    private Camera mainCamera;
    
    // Start is called before the first frame update
    private void Start()
    {
        mainCamera = Camera.main;
        startSteerWheelPoint = mainCamera.WorldToScreenPoint(m_steerWeelTransform.position); // мировые координаты переводим в экранные координаты
    }

    // Update is called once per frame
    void Update()
    {
    // если нажата левая кнопка мыши (аналог тач на мобильном)
        if (Input.GetMouseButton(0))
        {
            // угол между рулем и точкой касания экрана замеряем есть или нет
            float angle = Vector2.Angle(Vector2.up, (Vector2) Input.mousePosition - startSteerWheelPoint);
            angle /= m_maxSteerAngle; // пропорция текущего значения и максимального
            angle = Mathf.Clamp01(angle); // привожу число угла к диапазону 0 - 1 .. если 0. то 0 .. если больше 1 то 1
            
            // направо поворот или налево
            if (Input.mousePosition.x > startSteerWheelPoint.x)
            {
                angle *= -1f;
            }

            SteerAxis = angle;
        }
        else
        {
            SteerAxis = 0;
        }
        
        m_steerWeelTransform.localEulerAngles = new Vector3(0f, 0f, SteerAxis * m_maxSteerAngle); // визуальный поворот рулевого колеса
    }
}
