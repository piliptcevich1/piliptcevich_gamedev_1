﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FingerDriverScore : MonoBehaviour
{
    [SerializeField] private FingerDriverTrack m_nowSegment;
    
    public int fingerDriverScore;
    public Text fingerDriverText;
    public int checkSegment;
    private void Start()
    {
        fingerDriverScore = 0;
        checkSegment = 0;
    }

    private void Update()
    {
        if (checkSegment != m_nowSegment.nowSegment)
        {
            fingerDriverScore = fingerDriverScore + 10;
            fingerDriverText.text = "Score : " + fingerDriverScore.ToString();
            checkSegment = m_nowSegment.nowSegment;
            print(checkSegment);
            
        }
    }

    //нужно, чтобы сменился номер текущего сегмента .. тогда +1 к счетчику 
  
}
