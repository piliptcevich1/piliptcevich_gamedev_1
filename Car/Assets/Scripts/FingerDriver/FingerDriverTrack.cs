﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FingerDriverTrack : MonoBehaviour
{
    // описываем сегмент трассы (треугольник)
    private class TrackSegment
    {
        public Vector3[] Points;

        public bool IsPointInSegmet(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(
                point, Points[0], Points[1], Points[2]);
        }
    }

    [SerializeField] private LineRenderer m_lineRender;
    [SerializeField] private bool m_debug;
    
    private Vector3[] corners; // узловые точки трека (сферы)
    private TrackSegment[] segments;

    public int nowSegment;
    
    // Start is called before the first frame update
    private void Start()
    {
        nowSegment = 0;
        // заполняем массив опорными точками (сегментами)
        corners = new Vector3[transform.childCount]; //считаем количество детей того gameobj, на котором висит скрипт
        // бежим по созданному массиву:
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject; // записываем объект-ребенка
            corners[i] = obj.transform.position; // записываем в obj координату сферы (ребёнка)
            obj.GetComponent<MeshRenderer>().enabled = false; // получение компонента объекта
        }
        
        // настраиваем  Line Renderer
        m_lineRender.positionCount = corners.Length;
        m_lineRender.SetPositions(corners);
        
        //извлекаем сетку из треугольников
        Mesh mesh = new Mesh();
        m_lineRender.BakeMesh(mesh, true);  //true - координаты относительно локальных координат
        
        // создаем массив сегментов трассы
        // каждый треугольник описан 3-мя токами координат из массива mesh
        segments = new TrackSegment[mesh.triangles.Length / 3]; // 1й треугольник из первых трех вершин (3х цифр в массиве mesh)
        int segmentCounter = 0;
        // массив переходим с шагом 3 (запись ниже в i+=3)
        for (int i = 0; i < mesh.triangles.Length; i+=3)
        {
            segments[segmentCounter] = new TrackSegment();
            segments[segmentCounter].Points = new Vector3[3];
            segments[segmentCounter].Points[0] = mesh.vertices[mesh.triangles[i]]; // выбираем номера вершин треугольников
            segments[segmentCounter].Points[1] = mesh.vertices[mesh.triangles[i + 1]]; 
            segments[segmentCounter].Points[2] = mesh.vertices[mesh.triangles[i + 2]];
            segmentCounter++;
        }
        
        // отдельно можно продебажить сегменты
        if (!m_debug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = point;
                sphere.transform.localScale = Vector3.one * 0.1f;
            }
            
        }
    }

    /// <summary>
    /// Определяем нахождится ли точка на трассе
    /// </summary>
    /// <param name="point">точка</param>
    /// <returns></returns>
    public bool IsPointInTrack(Vector3 point)
    {
        foreach (var segment in segments)
        {
            if (segment.IsPointInSegmet(point))
            {
                for (int i = 0; i < segments.Length; i++)
                {
                    if (segment.Points == segments[i].Points)
                    {
                        nowSegment = i;
                        //print(nowSegment);
                        //break;
                    }

                    //print(i);
                }
                
                return true;
            }
        }

        return false;
    }

 }
