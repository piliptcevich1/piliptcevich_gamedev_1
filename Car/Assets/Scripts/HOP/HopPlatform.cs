﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Platfom
{
    ID = 0,
    posX = 0,
    posZ = 0,
}
public class HopPlatform : MonoBehaviour
{
    [SerializeField] private GameObject m_baseView;
    [SerializeField] private GameObject m_greenView;

    public void SetGreen()
    {
        m_baseView.SetActive(false);
        m_greenView.SetActive(true);
        Invoke("SetBase", 0.5f);
    }

    private void SetBase()
    {
        m_baseView.SetActive(true);
        m_greenView.SetActive(false);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        SetBase();
    }

    
}
