﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopInput : MonoBehaviour
{
    private float strafe;
    public float Strafe => strafe; //помогает проверить флоат прямо здесь на валидность
    private float screenCenter;
    
    
    // Start is called before the first frame update
    private void Start()
    {
        screenCenter = Screen.width * 0.5f; // просто чтобы было такое значение под рукой всегда .. половина ширины экрана .. доступен экран только после запуска игры
        
        
    }

    // Update is called once per frame
    private void Update()
    {
        if (!Input.GetMouseButton(0)) //если не нажата кнопка, то ничего не возвращаем
        {
            return;
        }

        var mousePosition = Input.mousePosition.x;
        if (mousePosition > screenCenter)
        {
            strafe = (mousePosition - screenCenter) / screenCenter;  //получаем отклонение от 0 до 1
        }
        else
        {
            strafe = 1 - mousePosition / screenCenter; 
            strafe *= -1f;  //нам нужно получить отрицательное значение, чтобы двигаться влево
        }
    }
}
