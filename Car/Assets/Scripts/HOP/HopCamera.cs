﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopCamera : MonoBehaviour
{
    [SerializeField] private Transform m_target; //цель за которой двигаемся

    [SerializeField] private float m_distanse = 4f; //дистанция с которой будем двигаться

    [SerializeField] private float m_height = 2f; //двигаемся на этой высоте
    

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = new Vector3(
            0f, m_height, m_target.position.z - m_distanse);
        transform.position = Vector3.Lerp(
            transform.position, pos, Time.deltaTime * 5f);
    }
}
