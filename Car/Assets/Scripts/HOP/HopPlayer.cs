﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HopPlayer : MonoBehaviour
{
    [SerializeField] private AnimationCurve m_JumpCurve; //создаем кривую анимации для привязки прыжка шарика
    [SerializeField] private float m_JumpHeight = 1f; //высота прыжка
    [SerializeField] private float m_JumpDistance = 2f; // длина прыжка
    [SerializeField] private float m_BallSpeed = 1f;
    
    [SerializeField] private HopInput m_Input;

    [SerializeField] private HopTrack m_Track; // делаем связи с другими классами через прямое указание на них

    private float interation; //цикл прыжка
    private float startZ; //точка начала прыжка
    
    // Update is called once per frame
    void Update()
    {

        //private float m_BallSpeed = m_Track.nearestPlatform.transform.position.z;
        
        var pos = transform.position; //запоминаем текущее положение, чтобы не вызывать его постоянно
        //смещение 
        pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f); //даем плавность 
        //прыжок
        pos.y = m_JumpCurve.Evaluate(interation) * m_JumpHeight; //Evaluate спрашивает положение по Х и возвращает из кривой значение Y
        pos.z = startZ + interation * m_JumpDistance;
        transform.position = pos; //пока еще прыгает родитель вместе со сферой
        interation += Time.deltaTime * m_BallSpeed;

        if (interation <1f)
        {
            return;  //если не добрались до концовки прыжка, ничего не делаем
        }

        interation = 0; //сбрасываем итерацию
        startZ += m_JumpDistance; //прибавляем к старту пройденную дистанцию, фиксируем по сути новый старт для прыжка
        if (m_Track.IsBallOnPlatform(transform.position))
        {
            if (m_Track.xPlatformForSave == false)
            {
                m_BallSpeed = 1.5f;
                m_JumpHeight = 1.5f;
                //m_BallSpeed = m_BallSpeed * 1.15f;

                // print(m_BallSpeed);
            }

            else
            {
                m_BallSpeed = 1f;
                m_JumpHeight = 1f;
                //m_BallSpeed = m_BallSpeed * 0.99f;
                // print(m_BallSpeed);
            }
            return; //если ок, мы сбрасываем параметры и начинаем новый прыжок
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //если не попали, тогда перегружаем сцену

    }
}
