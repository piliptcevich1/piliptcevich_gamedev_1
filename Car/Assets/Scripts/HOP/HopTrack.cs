﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private GameObject m_Platform;
    [SerializeField] private bool m_useRandomSeed; //выбор в настройках текущего класса включить или выключить сид рандома
    [SerializeField] private int m_seed = 123456;
    private List<GameObject> platforms = new List<GameObject>();
    
    //private float posForsave = 2f;

    //ublic float speedForSave = 1f;
    public bool xPlatformForSave = true;
    
    private void Start()
    {
        platforms.Add(m_Platform);
        
        //фиксируем вариант рандома
        if (m_useRandomSeed)
        {
            Random.InitState(m_seed); //если задано рандому использовать рандом сид, то зерно равно сид
        }
        
        for (int i = 0; i < 25; i++)
        {
            GameObject obj = Instantiate(m_Platform, transform); //доступ родителя к субклассу. создание копий субклассов на сцене
            Vector3 pos = Vector3.zero; //аналог ньювектор 3
            //pos.z = posForsave; // задаем случайный шаг для расстояния между платформами
            pos.z = 2 * (i + 1); //каждая следующая платформа создается с шагом 2
            pos.x = Random.Range(-1, 2); //берет случайное число от первого числа включительно до второго не включительно .. данный рандом -1, 0, 1
            // у рандома есть таблицп генерации .. потому если вызов первого числа одинаков, тогда абсолютно одинаковый ряд цифр в рандоме ("зерно рандома")
            obj.transform.position = pos;
            obj.name = $"Platform_{i + 1}"; //будет фиксироваться имя платформы
            platforms.Add(obj);
            
            //posForsave = posForsave + Random.Range(0, 3) + 2f;
        }
    }

    //находится ли шар на данной платформе
    public bool IsBallOnPlatform(Vector3 position)
    {
        position.y =
            0f; //y - движение вверх вниз .. родитель будет всегда двигаться по 0y, а ребенок будет типо прыгать, не мешая расчетам .. чтобы не заморачиваться на расчет движения по оси Y
        // находим ближайшую платформу .. проверяем от первой, сравниваем дистанцию со следующей, если меньше, то записываем в ближайший, проверяем так до конца массива
        var nearestPlatform = platforms[0];
        for (int i = 1; i < platforms.Count; i++)
        {
            var platformZ = platforms[i].transform.position.z;
            if (platformZ + 0.5f < position.z) //проверяем перелет .. эта платформа не подходит .. размер платформы 1
            {
                continue;
            }

            if (platformZ - position.z > 0.5f) // недолет
            {
                continue;
            }

            nearestPlatform = platforms[i]; //мы нашли ту, над которой шарик 
            if (nearestPlatform.transform.position.x < 0f)
            {
                xPlatformForSave = false; //повышаем скорость если платформа в левом ряду
            }

            else
            {
                xPlatformForSave = true;
            }
            break;
        }

        
        float minX = nearestPlatform.transform.position.x - 0.5f;
        float
            maxX = nearestPlatform.transform.position.x +
                   0.5f; //находим координаты по иксу платформы .. если мы в них попали, то мы попали в палтформу
        var platform = nearestPlatform.GetComponent<HopPlatform>();
        platform.SetGreen();
        
        //проверяем положение платформы, ускоряем и замедляем шарик
        //xPlatformForSave = nearestPlatform.transform.position.x;
        //print(xPlatformForSave);
        
        //if (nearestPlatform.transform.position.x < 0f) { //speedForSave = speedForSave * 1.3f; //повышаем скорость если платформа в левом ряду }
         
        //if (nearestPlatform.transform.position.x == 0f) {//speedForSave = speedForSave * 1f; //если по центру продолжаем с прежней скоростью}
        
        //if (nearestPlatform.transform.position.x > 0f) {//speedForSave = speedForSave * 0.7f; //замедляем если платформа справа}
        
        return position.x > minX && position.x < maxX;
    }
}
