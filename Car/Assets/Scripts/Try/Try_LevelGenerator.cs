﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Try_LevelGenerator : MonoBehaviour
{
    [SerializeField] private Try_Enemy m_Enemy;
    [SerializeField] private int m_EnemyCouter;
    [SerializeField] private Try_Player m_Player;
    
    private List<Transform> enemies;
    private int sizeX;
    private int sizeY;
    private int sizeZ;
    private float distance = 10f;
    private int postionX = 0;
    private GameObject prefab;
    
    // Start is called before the first frame update
    void Start()
    {
        EnemyGenerator();
        PlayerGenerator();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void PlayerGenerator()
    {
        m_Player = Instantiate(m_Player, new Vector3(0f,0f,0f), Quaternion.identity);
    }
    
    private void EnemyGenerator()
    {
        for (int i = 0; i < (m_EnemyCouter + 1); i++)
        {
           
            m_Enemy = Instantiate(m_Enemy, new Vector3(postionX,0,distance), Quaternion.identity);
            distance = distance + 20f;
            postionX = Random.Range(-4, 4);
        }
    }
}
